package org.gcube.vremanagement.contextmanager.model.types;

import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;

public interface ContextRetriever {

	Context getContextById(String id) throws InvalidContextException;
}
