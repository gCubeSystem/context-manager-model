package org.gcube.vremanagement.contextmanager.model.operators.context;

import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidParameterException;
import org.gcube.vremanagement.contextmanager.model.exceptions.OperationException;
import org.gcube.vremanagement.contextmanager.model.operators.OperatorParameters;
import org.gcube.vremanagement.contextmanager.model.report.OperationResult;
import org.gcube.vremanagement.contextmanager.model.report.ReportEntry;
import org.gcube.vremanagement.contextmanager.model.types.Context;

public abstract class CustomContextOperator<T, P extends OperatorParameters> implements ContextOperator {
	
	protected abstract P checkAndTrasformParameters(Context context, OperatorParameters params) throws InvalidParameterException;
	
	protected abstract T execute(Context context, P params) throws OperationException;
	
	public abstract T undo(Context context, OperatorParameters params);
	
	public ReportEntry<T> run(Context context, P params){
		P operationParams = null;
		
		try {
			operationParams = checkAndTrasformParameters(context, params);
		} catch (InvalidParameterException e) {
			return new ReportEntry<T>(this.getOperationId(), this.getDescription(), OperationResult.failure("parameters error: "+e.getMessage()), null);	
		} catch (Throwable e) {
			return new ReportEntry<T>(this.getOperationId(), this.getDescription(), OperationResult.failure("unexpected parameters error: "+e.getMessage()), null);	
		}
		
		T result = null;
		try {
			result = execute(context, operationParams);
		}catch (OperationException e) {
			return new ReportEntry<T>(this.getOperationId(), this.getDescription(), OperationResult.failure("operation error: "+e.getMessage()), null);	
		}  catch (Throwable e) {
			return new ReportEntry<T>(this.getOperationId(), this.getDescription(), OperationResult.failure("unexpected operation error: "+e.getMessage()), null);	
		}
		
		return new ReportEntry<T>(this.getOperationId(), this.getDescription(), OperationResult.success() , result);
	}
}
