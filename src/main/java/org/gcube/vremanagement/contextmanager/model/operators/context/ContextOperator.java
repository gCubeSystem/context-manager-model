package org.gcube.vremanagement.contextmanager.model.operators.context;

import java.util.Set;

import org.gcube.vremanagement.contextmanager.model.types.Context;

public interface ContextOperator {
	
	Set<Context.Type> getAllowedContextType();
	
	String getOperationId();
	
	String getDescription();	

}
