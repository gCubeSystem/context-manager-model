package org.gcube.vremanagement.contextmanager.model.report;

import java.util.Arrays;
import java.util.List;

public class OperationResult {

	public static OperationResult failure(String... errors ){
		return new OperationResult(Arrays.asList(errors));
	}
	
	public static OperationResult success(){
		return new OperationResult();
	}
	
	public enum Status {
		SUCCESS,
		FAILURE
	}
	
	private Status status;
	private List<String> errors;
	
	private OperationResult() {	
		this.status = Status.SUCCESS;
	}
	
	private OperationResult(List<String> errors) {
		super();
		this.status = Status.FAILURE;
	}


	public boolean isSuccess() {
		return status==Status.SUCCESS;
	}

	public List<String> getErrors() {
		return errors;
	}
	
}
