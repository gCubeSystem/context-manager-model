package org.gcube.vremanagement.contextmanager.model.exceptions;

public class InvalidContextException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5837397038445859796L;

	public InvalidContextException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidContextException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidContextException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
