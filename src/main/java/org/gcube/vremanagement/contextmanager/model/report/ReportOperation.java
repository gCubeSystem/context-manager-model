package org.gcube.vremanagement.contextmanager.model.report;

public interface ReportOperation {

	default boolean isSingleOperation() {
		return false;
	}
}
