package org.gcube.vremanagement.contextmanager.model.types;

public class Context {
	
	
	private String id;
	private String name;
	private Context parent;
	
	public enum Type {
		INFRASTRUCTURE(null), 
		VO(INFRASTRUCTURE), 
		VRE(VO);
		
		private Type parent;
		
		Type(Type parent) {
			this.parent = parent;
		}
		
		public Type getPossibleParent() {
			return this.parent;
		}
	}
	
	private Type type;
	
	public Context(Context parent, String id, String name, Type type) {
		super();
		this.id = id;
		this.name = name;
		this.type = type; 
		this.parent = parent;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}
	
	public Context getParent() {
		return parent;
	}

	@Override
	public String toString() {
		return "Context [id=" + id + ", name=" + name + ", type=" + type + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Context other = (Context) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

}
