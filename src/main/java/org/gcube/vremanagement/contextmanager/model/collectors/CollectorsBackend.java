package org.gcube.vremanagement.contextmanager.model.collectors;

import java.util.List;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.vremanagement.contextmanager.model.types.Context;

public interface CollectorsBackend {

	Resource find(String resourceId);
	
	void createContext(Context context, String parentContextId, List<String> resourceIds);
	
	boolean removeContext(Context context);
	
	boolean addResourceToContext(Context context, Resource resource) throws Exception;
	
	boolean removeResourceFromContext(Context context, Resource resource);
	
	boolean updateResource(Resource resource);
	
}
