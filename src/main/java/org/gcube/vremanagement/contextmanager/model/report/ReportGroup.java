package org.gcube.vremanagement.contextmanager.model.report;

import java.util.LinkedList;
import java.util.List;

public class ReportGroup implements ReportOperation{

	private String name;
	
	protected ReportGroup() {}
	
	private List<ReportEntry> entries = new LinkedList<>();
	
	public ReportGroup(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void addEntry(ReportEntry entry) {
		entries.add(entry);
	}
}
