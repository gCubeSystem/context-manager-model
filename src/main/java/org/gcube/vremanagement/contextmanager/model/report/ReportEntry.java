package org.gcube.vremanagement.contextmanager.model.report;

import org.gcube.vremanagement.contextmanager.model.operators.OperatorParameters;

public class ReportEntry<T> implements ReportOperation{

	private String operationId;
	private String description;
	private OperatorParameters parameters;
	private OperationResult result;
	private T entryDescriptor;
	
	protected ReportEntry() {}

	public ReportEntry(String operationId, String description, OperationResult result, T entryDescriptor) {
		super();
		this.operationId = operationId;
		this.description = description;
		this.result = result;
		this.entryDescriptor = entryDescriptor;
	}

	public ReportEntry(String operationId, String description, OperationResult result, OperatorParameters parameters, T entryDescriptor) {
		this(operationId, description, result, entryDescriptor);
		this.parameters = parameters;
	}
	
	public String getOperationId() {
		return operationId;
	}

	public String getDescription() {
		return description;
	}

	public OperatorParameters getParameters() {
		return parameters;
	}

	public OperationResult getResult() {
		return result;
	}

	@Override
	public boolean isSingleOperation() {
		return true;
	}

	public T getEntryDescriptor() {
		return entryDescriptor;
	}
	
}
