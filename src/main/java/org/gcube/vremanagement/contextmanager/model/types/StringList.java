package org.gcube.vremanagement.contextmanager.model.types;

import java.util.List;

public class StringList {

	private List<String> values;
	
	protected StringList() {}
	
	public StringList(List<String> values) {
		super();
		this.values = values;
	}

	public List<String> getValues() {
		return values;
	}
	
}
