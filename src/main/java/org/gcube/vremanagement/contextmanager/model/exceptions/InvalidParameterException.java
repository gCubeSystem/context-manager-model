package org.gcube.vremanagement.contextmanager.model.exceptions;

public class InvalidParameterException extends OperationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public InvalidParameterException(String message) {
		super(message);
	}

	
}
