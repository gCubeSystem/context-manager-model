package org.gcube.vremanagement.contextmanager.model.report;

import java.util.LinkedList;
import java.util.List;

import org.gcube.vremanagement.contextmanager.model.types.Context.Type;

public class Report {

	private String contextId;
	private String contextName;
	private Type contextType;
	private String operationName;
	private String user;
	
	private OperationResult result;
	
	private List<ReportOperation> entries = new LinkedList<>();
	
	protected Report() {}
	
	public Report(String contextId, String contextName, Type contextType, String operationName, String user) {
		super();
		this.contextId = contextId;
		this.contextName = contextName;
		this.contextType = contextType;
		this.operationName = operationName;
		this.user = user;
	}
	
	public String getContextId() {
		return contextId;
	}

	public String getContextName() {
		return contextName;
	}

	public Type getContextType() {
		return contextType;
	}

	public String getOperationName() {
		return operationName;
	}

	public String getUser() {
		return user;
	}

	public List<ReportOperation> getEntries() {
		return entries;
	}
	
	public void add(ReportOperation operation) {
		entries.add(operation);
	}

	public OperationResult getResult() {
		return result;
	}

	public void setResult(OperationResult result) {
		this.result = result;
	}
}
