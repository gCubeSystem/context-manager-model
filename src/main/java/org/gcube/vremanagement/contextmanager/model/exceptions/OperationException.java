package org.gcube.vremanagement.contextmanager.model.exceptions;

public abstract class OperationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OperationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public OperationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public OperationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
