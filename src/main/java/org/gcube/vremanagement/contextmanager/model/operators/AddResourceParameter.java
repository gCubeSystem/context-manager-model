package org.gcube.vremanagement.contextmanager.model.operators;

public class AddResourceParameter implements OperatorParameters{

	private String contextId;
	private String resourceId;
	
	protected AddResourceParameter() {}
	
	public AddResourceParameter(String contextId, String resourceId) {
		super();
		this.contextId = contextId;
		this.resourceId = resourceId;
	}

	public String getContextId() {
		return contextId;
	}

	public String getResourceId() {
		return resourceId;
	}
	
}
