package org.gcube.vremanagement.contextmanager.model.operators.context;

import org.gcube.vremanagement.contextmanager.model.report.OperationResult;
import org.gcube.vremanagement.contextmanager.model.types.Context;

public interface MandatoryContextOperator extends ContextOperator {

	OperationResult onCreate(Context context);
	
	OperationResult onDispose(Context context);
	
}
